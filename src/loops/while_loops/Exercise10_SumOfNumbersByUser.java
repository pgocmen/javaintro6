package loops.while_loops;

import utilities.ScannerHelper;

public class Exercise10_SumOfNumbersByUser {
    public static void main(String[] args) {


        System.out.println("\n-----------Fori Loop-----------\n");

        int sum = 0;

        for (int i = 1; i <= 5; i++) {
            sum += ScannerHelper.getANumber();}

        System.out.println(sum);



        System.out.println("\n-----------While Loop-----------\n");

        int start = 1;
        int sumWhile = 0;

        while (start <= 5){
            sumWhile += ScannerHelper.getANumber();
            start++;}

        System.out.println(sumWhile);

    }
}
