package loops;

public class UnderstandingLoops {
    public static void main(String[] args) {

        // Print Hello World 5 times

        /*
        for loop syntax

        for(initialization; termination; update) {
            // block of code to be executed}


            initialization -> start point
            termination -> stop condition
            update -> increasing or decreasing -> increment and decrement operators

         */

        // Print Hello World! 5 times -> 0, 1, 2, 3, 4

        for (int i = 0; i < 5; i++) {
            System.out.println("Hello World!");
        }

    }
}
