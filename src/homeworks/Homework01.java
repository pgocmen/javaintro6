package homeworks;

public class Homework01 {
    public static void main(String[] args) {

        System.out.println("-----------Task-1--------------\n");
       /* -Find the binary representation of below words
        J   -> 	01001010
        A   ->  01000001
        V   ->  01010110
        A   ->  01000001

        S   ->  01010011
        E   ->  01000101
        L   ->  01001100
        E   ->  01000101
        N   ->  01001110
        I   ->  01001001
        U   ->  01010101
        M   ->  01001101
        */

        System.out.println("\n-----------Task-2--------------\n");
        /*
        -Find the decimal values of given binary code below and their matching letters in the ASCII Table
        01001101        -> 77 -> M
        01101000        -> 104 -> h
        01111001        -> 121 -> y
        01010011        -> 83 -> S
        01101100        -> 108 -> l
         */


        System.out.println("\n-----------Task-3--------------\n");


        System.out.println("I start to practice \"JAVA\" today, and I like it.");
        System.out.println("The secret of getting ahead is getting started.");
        System.out.println("\"Don't limit yourself.\"");
        System.out.println("Invest in your dreams. Grind now. Shine later.");
        System.out.println("It’s not the load that breaks you down, it’s the way you carry it.");
        System.out.println("The hard days are what make you stronger.");
        System.out.println("You can waste your lives drawing lines. Or you can live your life crossing them.");



        System.out.println("\n-----------Task-4--------------\n");

        System.out.println("\tJava is easy to write and easy to run—this is the foundational\n" +
                "strength of Java and why many developers program in it. When you\n" +
                "write Java once, you can run it almost anywhere at any time.\n\n" +
                "\tJava can be used to create complete applications that can run on\n" +
                "a single computer or be distributed across servers and clients in a\n" +
                "network.\n\n" +
                "\tAs a result, you can use it to easily build mobile applications or\n" +
                "run-on desktop applications that use different operating systems and\n" +
                "servers, such as Linux or Windows.");


        System.out.println("\n-----------Task-5--------------\n");

        byte myAge = 28;
        char myFavoriteNumber = '8';
        double myHeight = 5.5;
        char myFavoriteLetter = 'P';

        System.out.println("My age is = " + myAge);
        System.out.println("My favorite number is = " + myFavoriteNumber);
        System.out.println("My height is = " + myHeight + "\"");
        System.out.println("My favorite letter is  = " + myFavoriteLetter);
    }
}
