package homeworks;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Homework04 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("--------------------Task1------------------------");

        String name = ScannerHelper.getAName();
        System.out.println("The length of the name is = " + name.length());
        System.out.println("The first character in the name is = " + name.charAt(0));
        System.out.println("The last character in the name is = " + name.charAt(name.length()-1));
        System.out.println("The first 3 characters in the name are = " + name.substring(0,3));
        System.out.println("The last 3 characters in the name are = " + name.substring(name.length()-3));
        if(name.charAt(0)=='a'||name.charAt(0)=='A') System.out.println("You are in the club!");
        System.out.println("Sorry, you are not in the club");

        System.out.println("--------------------Task2------------------------");

        String address = ScannerHelper.getAnAddress();
        if(address.toLowerCase().contains("chicago")){
            System.out.println("You are in the club");}
        else if(address.toLowerCase().contains("des plaines")){
            System.out.println("You are welcome to join to the club");}
        else
            System.out.println("Sorry, you will never be in the club");

        System.out.println("--------------------Task3------------------------");

        System.out.println("User please enter  your favorite country");
        String favCountry = input.nextLine();

        if (favCountry.toLowerCase().contains("a") && favCountry.toLowerCase().contains("i")){
            System.out.println("a and i are there");
        } else if (favCountry.toLowerCase().contains("a")) {
            System.out.println("a is there");
        } else if (favCountry.toLowerCase().contains("i")) {
            System.out.println("i is there");
        } else{
            System.out.println("a and i are not there");}


        System.out.println("--------------------Task4------------------------");

        String str = "Java is FUN";
        System.out.println("The first word in the str is = " + str.substring(0,str.indexOf(" ")));
        System.out.println("The second word in the str is = " + str.substring(str.indexOf(" ")+1, str.lastIndexOf(" ")));
        System.out.println("The third word in the str is = " + str.substring(str.toLowerCase().lastIndexOf(" ") +1));

        System.out.println("--------------------EndofProgram------------------------");

    }
}
