package homeworks;

import java.util.Scanner;

public class Homework02 {
    public static void main(String[] args) {

        System.out.println("------------------TASK-1--------------------");

        Scanner input = new Scanner(System.in);

        System.out.println("User please enter your first number");
        int fNum = input.nextInt();

        System.out.println("User please enter your second number");
        int sNum = input.nextInt();

        System.out.println("The number 1 entered by user is = " + fNum);
        System.out.println("The number 2 entered by user is = " + sNum);
        System.out.println("The sum of number 1 and 2 entered by user is = " + (fNum + sNum));


        System.out.println("\n-----------------TASK-2--------------------\n");

        System.out.println("User please enter two numbers");
        int num1 = input.nextInt();
        int num2 = input.nextInt();



        System.out.println("The product of the given 2 numbers is: " + (num1 * num2));


        System.out.println("\n------------------TASK-3--------------------\n");

        System.out.println("User please enter two floating numbers");
        double num3 = input.nextDouble();
        double num4 = input.nextDouble();


        System.out.println("The sum of the given numbers is = " + (num3 + num4));
        System.out.println("The product of the given numbers is = " + (num3 * num4));
        System.out.println("The subtraction of the given numbers is = " + (num3 - num4));
        System.out.println("The division of the given numbers is = " + (num3 / num4));
        System.out.println("The remainder of the given numbers is = " + (num3 % num4));


        System.out.println("\n------------------TASK-4--------------------\n");

        System.out.println(-10 + 7 * 5);
        System.out.println((72 + 24) % 24);
        System.out.println(10 + -3 * 9 / 9);
        System.out.println(5 + 18 / 3 * 3 - (6 % 3));

        System.out.println("\n------------------TASK-5--------------------\n");

        System.out.println("User please enter your two numbers");
        int num5 = input.nextInt();
        int num6 = input.nextInt();

        System.out.println("The average of the given numbers is: " + (num5 + num6) / 2);



        System.out.println("\n------------------TASK-6--------------------\n");

        System.out.println("User please enter 5 numbers");
        int num7 = input.nextInt();
        int num8 = input.nextInt();
        int num9 = input.nextInt();
        int num10 = input.nextInt();
        int num11 = input.nextInt();

        System.out.println((num7+num8+num9+num10+num11) / 5);


        System.out.println("\n------------------TASK-7--------------------\n");

        System.out.println("User please enter 3 numbers");
        int num12 = input.nextInt();
        int num13 = input.nextInt();
        int num14 = input.nextInt();

        System.out.println("The " + num12 + " multiplied with " + num12 + " is = " + (num12 * num12));
        System.out.println("The " + num13 + " multiplied with " + num13 + " is = " + (num13 * num13));
        System.out.println("The " + num14 + " multiplied with " + num14 + " is = " + (num14 * num14));


        System.out.println("\n------------------TASK-8--------------------\n");

        System.out.println("User please enter the side of a square");
        int squareSide = input.nextInt();
        input.nextLine();

        System.out.println("Perimeter of the square = " + (4 * squareSide));
        System.out.println("Area of the square = " + (squareSide * squareSide));



        System.out.println("\n------------------TASK-9--------------------\n");

        double salary = 90000;

        System.out.println("A Software Engineer in Test can earn $" + (salary * 3) + " in 3 years.");


        System.out.println("\n------------------TASK-10--------------------\n");

        System.out.println("User please enter your favorite book");
        String fBook = input.nextLine();

        System.out.println("User please enter your favorite color");
        String fColor = input.nextLine();

        System.out.println("User please enter your favorite number");
        int num = input.nextInt();
        input.nextLine();

        System.out.println("User's favorite book is: " + fBook + "\nUser's favorite color is: " + fColor +
                "\nUser's favorite number is: " + num);


        System.out.println("\n------------------TASK-11--------------------\n");

        System.out.println("User please enter your first name");
        String firstName = input.next();

        System.out.println("User please enter your last name");
        String lastName = input.next();

        System.out.println("User please enter your age");
        int age = input.nextInt();
        input.nextLine();

        System.out.println("User please enter your email address");
        String eAddress = input.nextLine();

        System.out.println("User please enter your phone number");
        String pNumber = input.nextLine();

        System.out.println("User please enter your address");
        String address = input.nextLine();

        System.out.println("\tUser who joined this program is " + firstName + " " + lastName + ". " +
                firstName + "'s  age is \n" + age + ". " + firstName + "'s email address is " + eAddress +
                ", phone number\n is " + pNumber + ", and address is " + address + ".");


    }
}
