package homeworks;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Homework05 {
    public static void main(String[] args) {


        System.out.println("\n-------------------Task-1--------------------------\n");

        for (int i = 1; i <= 100; i++) {
            if (i == 98) System.out.println(i);
            else if (i % 7 == 0) { System.out.print(i + " - ");}
        }

        System.out.println("\n-------------------Task-2--------------------------\n");

        for (int i = 1; i <= 50; i++) {
            if (i == 48) System.out.println(i);
            else if (i % 2 == 0 && i % 3 == 0) System.out.print(i + " - ");
        }

        System.out.println("\n-------------------Task-3--------------------------\n");

        for (int i = 100; i >= 50; i--) {
            if (i == 50) {
                System.out.println(i);
            } else if (i % 5 == 0) {
                System.out.print(i + " - "); }
        }

        System.out.println("\n-------------------Task-4--------------------------\n");

        for (int i = 0; i <= 7; i++) {
            System.out.println("The square root of " + i + " is = " + (i * i));
        }

        System.out.println("\n-------------------Task-5--------------------------\n");

        int count = 0;

        for (int i = 1; i <= 10; i++) {
            count += i;
        }
        System.out.println(count);

        System.out.println("\n-----------------------------Task-6------------------------------");

        Scanner input = new Scanner(System.in);

        System.out.println("User please enter a positive number");
        int num1 = input.nextInt();
        int num2 = 1;

        if (num1 == 0) System.out.println("1");
        for (int i = 1; i <= num1; i++) {
            num2 = num2*i;}
        System.out.println(num2);




        System.out.println("\n-----------------------------Task-7------------------------------");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter your full name:");
        String name2 = scanner.nextLine();

        int countVowel = 0;

        for (int i = 0; i < name2.length(); i++) {
            if (name2.toLowerCase().charAt(i) == 'a' || name2.toLowerCase().charAt(i) == 'e' || +
                    name2.toLowerCase().charAt(i) == 'i' || name2.toLowerCase().charAt(i) == 'o' || name2.toLowerCase().charAt(i) == 'u')

                countVowel++;
        }
        System.out.println("There are " + countVowel + " vowel letters in this full name");


        System.out.println("\n-----------------------------Task-8------------------------------");


        while (true){
            String name = ScannerHelper.getAName();
            if (name.startsWith("j") || name.startsWith("J"))
                System.out.println("End of the Program");}




    }
}
