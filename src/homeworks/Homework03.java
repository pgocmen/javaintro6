package homeworks;


import java.util.Scanner;

public class Homework03 {
    public static void main(String[] args){

        System.out.println("------------------TASK-1--------------------");

        Scanner input = new Scanner(System.in);

        System.out.println("User please enter 2 numbers");
        int num1 = input.nextInt();
        int num2 = input.nextInt();

        System.out.println("The difference between the numbers is = " + (Math.abs(-5 - 27)));

        System.out.println("\n------------------TASK-2--------------------\n");

        System.out.println("User please enter 5 numbers");
        int nums1 = input.nextInt();
        int nums2 = input.nextInt();
        int nums3 = input.nextInt();
        int nums4 = input.nextInt();
        int nums5 = input.nextInt();

        System.out.println("Max value = " + Math.max(Math.max(nums1, nums2) , Math.max(nums3, Math.max(nums4,nums5))));
        System.out.println("Min value = " + Math.min(Math.min(nums1, nums2) , Math.min(nums3, Math.min(nums4,nums5))));

        System.out.println("\n------------------TASK-3--------------------\n");

        int randomNumber = (int)(Math.random() * (100 - 50 + 1) + 50);
        int randomNumber2 = (int)(Math.random() * (100 - 50 + 1) + 50);
        int randomNumber3 = (int)(Math.random() * (100 - 50 + 1) + 50);

        System.out.println("Number 1 = " + randomNumber);
        System.out.println("Number 2 = " + randomNumber2);
        System.out.println("Number 3 = " + randomNumber3);
        System.out.println("The sum of numbers is = " + (randomNumber2 + randomNumber + randomNumber3));

        System.out.println("\n-------------------------Task-4------------------------------------");

        double alexMoney = 125, mikeMoney = 220;
        alexMoney -= 25.5;
        mikeMoney += 25.5;


        System.out.println("Alex's money: $" + alexMoney);
        System.out.println("Mike's money: " + mikeMoney);

        System.out.println("\n-------------------------Task-5------------------------------------");

        double priceBicycle = 390, dailySave = 15.60;

        System.out.println((int)(priceBicycle/dailySave));


        System.out.println("\n-------------------------Task-6------------------------------------");

        String s1 = "5", s2 = "10";

        int w1 = Integer.parseInt(s1), w2 = Integer.parseInt(s2);

        System.out.println("Sum of " + w1 + " and " + w2 + " is = " + (w1 + w2) );
        System.out.println("Product of " + w1 + " and " + w2 + " is = " + (w1 * w2) );
        System.out.println("Division of " + w1 + " and " + w2 + " is = " + (w1 / w2) );
        System.out.println("Subtraction of " + w1 + " and " + w2 + " is = " + (w1 - w2) );
        System.out.println("Remainder of " + w1 + " and " + w2 + " is = " + (w1 % w2) );


        System.out.println("\n-------------------------Task-7------------------------------------");

        String s01 = "200", s02 = "-50";

        int i01 = Integer.parseInt(s01), i02 = Integer.parseInt(s02);

        System.out.println("The greatest value is = " + Math.max(i01 , i02));
        System.out.println("The greatest value is = " + Math.min(i01 , i02));
        System.out.println("The greatest value is = " + (i01 + i02) / 2);
        System.out.println("The greatest value is = " + Math.abs(i01 - i02));


        System.out.println("\n-------------------------Task-8------------------------------------");

        double dailySaving = .96;

        System.out.println((int)(24 / dailySaving ) + " days");
        System.out.println((int)(168 / dailySaving ) + " days");
        System.out.println("$" + (30 * 5) * dailySaving);



        System.out.println("\n-------------------------Task-9------------------------------------");
        double dailySaving2 = 62.5;

        System.out.println((int)(1250 / dailySaving2));


        System.out.println("\n-------------------------Task-10------------------------------------");

        double carPrice = 14_265;
        System.out.println("Option 1 will take " + (int)(carPrice / 475.50) + " months");
        System.out.println("Option 2 will take " + (int)(carPrice / 951) + " months");

        System.out.println("\n-------------------------Task-11------------------------------------");

        System.out.println("User please enter two numbers");
        int numbe1 = input.nextInt(), numbe2 = input.nextInt();

        double p1 = numbe1;
        double p2 = numbe2;

        System.out.println(p1/p2);

        System.out.println("\n-------------------------Task-12------------------------------------");
        int randomNum = (int) (Math.random() * 101);
        int randomNum2 = (int) (Math.random() * 101);
        int randomNum3 = (int) (Math.random() * 101);

        System.out.println(randomNum);
        System.out.println(randomNum2);
        System.out.println(randomNum3);


        if (randomNum <= 25 || randomNum2 <= 25 || randomNum3 <= 25) {
            System.out.println("false");}
        else {
            System.out.println("true");
        }


        System.out.println("\n--------------------------Task-13------------------------------------");


        System.out.println("User please enter a number between 1 and 7");
        int userNumber = input.nextInt();


        if (userNumber == 1){
            System.out.println("Monday");}
        else if (userNumber == 2) {
            System.out.println("Tuesday");}
        else if (userNumber == 3) {
            System.out.println("Wednesday");}
        else if (userNumber == 4) {
            System.out.println("Thursday");}
        else if (userNumber == 5) {
            System.out.println("Friday");}
        else if (userNumber == 6) {
            System.out.println("Saturday");}
        else if (userNumber == 7) {
            System.out.println("Sunday");}


        System.out.println("\n-------------------------Task-14------------------------------------");

        System.out.println("Tell me your exam results?");
        int test1 = input.nextInt() , test2 = input.nextInt(), test3 = input.nextInt();

        int testAverage = (test1 + test2 + test3) / 3;

        if (testAverage >= 70){
            System.out.println("YOU PASSED!");}

        else {
            System.out.println("YOU FAILED");}

        System.out.println("\n-------------------------Task-15------------------------------------");

        System.out.println("Enter 3 numbers");
        int numbers1 = input.nextInt(), numbers2 = input.nextInt(), numbers3 = input.nextInt();

        if (numbers1 == numbers2 & numbers2 == numbers3 ){
            System.out.println("TRIPLE MATCH");}
        else if (numbers1 == numbers2 | numbers2 == numbers3 | numbers3 == numbers1){
            System.out.println("DOUBLE MATCH");}
        else {
            System.out.println("NO MATCH");}

    }}
