package homeworks;

import java.util.Arrays;
import java.util.Scanner;

public class Homework06 {
    public static void main(String[] args) {

         System.out.println("\n----------Task 1----------\n");

         int[] numbers = new int[10];

            numbers[2] = 23;
            numbers[4] = 12;
            numbers[7] = 34;
            numbers[9] = 7;
            numbers[6] = 15;
            numbers[0] = 89;

            System.out.println(numbers[3]);
            System.out.println(numbers[0]);
            System.out.println(numbers[9]);
            System.out.println(Arrays.toString(numbers));

            System.out.println("\n----------Task 2----------\n");

            String[] variables = new String[5];

            variables[1] = "abc";
            variables[4] = "xyz";
            System.out.println(variables[3]);
            System.out.println(variables[0]);
            System.out.println(variables[4]);
            System.out.println(Arrays.toString(variables));

            System.out.println("\n----------Task 3----------\n");

            int[] numbers1 = {23, -34, -56, 0, 89, 100};

            System.out.println(Arrays.toString(numbers1));
            Arrays.sort(numbers1);
            System.out.println(Arrays.toString(numbers1));

            System.out.println("\n----------Task 4----------\n");

            String[] countries = {"Germany", "Argentina", "Ukraine", "Romania"};
            System.out.println(Arrays.toString(countries));
            Arrays.sort(countries);
            System.out.println(Arrays.toString(countries));

            System.out.println("\n----------Task 5----------\n");

            String[] cartoons = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};

            System.out.println(Arrays.toString(cartoons));

            Arrays.sort(cartoons);
            System.out.println(Arrays.binarySearch(cartoons, "Pluto") >= 0);

            System.out.println("\n----------Task 6----------\n");

            String[] cats = {"Garfield", "Tom", "Sylvester", "Azrael"};
            Arrays.sort(cats);
            System.out.println(Arrays.toString(cats));

            boolean hasCats = false;

            for (String c : cats) {
                if (c.equals("Garfield") && c.equals("Felix")) {
                    hasCats = true;
                    break;
                }
            }
            System.out.println(hasCats);

            System.out.println("\n----------Task 7----------\n");

            double[] doubles = {10.5, 20.75, 70, 80, 15.75};

            System.out.println(Arrays.toString(doubles));

            for (double d : doubles) {
                System.out.println(d);
            }

            System.out.println("\n----------Task 8----------\n");

            char[] chars = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};

            System.out.println(Arrays.toString(chars));

            int letterCount = 0, upperCount = 0, lowerCount = 0, digitCount = 0, special = 0;

            for (char c : chars) {
                if (Character.isLetter(c)) {
                    letterCount++;
                    if (Character.isUpperCase(c)) {
                        upperCount++;
                    } else {
                        lowerCount++;
                    }
                } else if (Character.isDigit(c)) {
                    digitCount++;
                } else {
                    special++;
                }
            }
            System.out.println("Letters = " + letterCount);
            System.out.println("Uppercase letters = " + upperCount);
            System.out.println("Lowercase letters = " + lowerCount);
            System.out.println("Digits = " + digitCount);
            System.out.println("Special characters = " + special);

            System.out.println("\n----------Task 9----------\n");

            String[] objects = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};
            System.out.println(Arrays.toString(objects));

            int upperCase = 0, lowerCase = 0, bOrP = 0, bookOrPen = 0;


            for (int i = 0; i < objects.length; i++) {
                if (Character.isUpperCase(objects[i].charAt(0))) {
                    upperCase++;
                }
                if (Character.isLowerCase(objects[i].charAt(0))) {
                    lowerCase++;
                }
                if (objects[i].toLowerCase().charAt(0) == 'b' || objects[i].toLowerCase().charAt(0) == 'p') {
                    bOrP++;
                }
                if (objects[i].contains("pen") || objects[i].contains("book")) {
                    bookOrPen++;
                }

                System.out.println("Elements starts with uppercase = " + upperCase);
                System.out.println("Elements starts with lowercase = " + lowerCase);
                System.out.println("Elements starting with B or P = " + bOrP);
                System.out.println("Elements having \"book\" or \"pen\" = " + bookOrPen);


                System.out.println("\n----------Task 10----------\n");

                int[] numbers2 = {3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78};
                System.out.println(Arrays.toString(numbers2));

                int greater = 0, less = 0, is10 = 0;

                for (int n : numbers2) {
                    if (n > 10) {
                        greater++;
                    }
                    if (n < 10) {
                        less++;
                    }
                    if (n == 10) {
                        is10++;
                    }
                }
                System.out.println("Elements that are more than 10 = " + greater);
                System.out.println("Elements that are less than 10 = " + less);
                System.out.println("Elements that are 10 = " + is10);
            }

            System.out.println("\n----------Task 11----------\n");
            int[] first = {5, 8, 13, 1, 2};
            int[] second = {9, 3, 67, 1, 0};

            System.out.println(Arrays.toString(first));
            System.out.println(Arrays.toString(second));

            int new1 = Math.max(first[0], second[0]);
            int new2 = Math.max(first[1], second[1]);
            int new3 = Math.max(first[2], second[2]);
            int new4 = Math.max(first[3], second[3]);
            int new5 = Math.max(first[4], second[4]);


            int[] storeNumbers3 = {new1, new2, new3, new4, new5};

            System.out.println(Arrays.toString(storeNumbers3));
    }
}
