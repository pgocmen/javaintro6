package operators.increment_decrement_operators;

import java.util.Scanner;

public class Exercise01 {
    public static void main(String[] args) {
        /*
        Write a program that asks user to enter a number
        Find the multiplication of the number from 1 to 5

        Assume they enter 13
        13 * 1 = 13
        13 * 2 = 26
        13 * 3 = 39
        13 * 4 = 52
        13 * 5 = 65
         */

        Scanner input = new Scanner(System.in);

        System.out.println("User please enter a number");
        int num = input.nextInt();
        int num2 = 1;

        System.out.println(num + " * " +  num2 + " = " + num * num2++);
        System.out.println(num + " * " +  num2 + " = " + num * num2++);
        System.out.println(num + " * " +  num2 + " = " + num * num2++);
        System.out.println(num + " * " +  num2 + " = " + num * num2++);
        System.out.println(num + " * " +  num2 + " = " + num * num2);





    }
}
