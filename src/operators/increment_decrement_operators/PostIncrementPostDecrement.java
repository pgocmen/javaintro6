package operators.increment_decrement_operators;

public class PostIncrementPostDecrement {
    public static void main(String[] args) {

        int num1 = 10, num2 = 10;

        System.out.println(num1++);

        System.out.println(++num2);


        int i1 = 10;

        --i1;

        i1--;
        System.out.println(--i1);
    }
}
