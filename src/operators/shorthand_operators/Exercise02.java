package operators.shorthand_operators;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {

        /*
        Write a Java program that asks user to enter their balance and one day transactions.
        Subtract each transaction from balance and return new balance using shorthand operator


        Requirements:
        Use Scanner class to read input from user

        Test data:
        Balance = $100.00
        1st transaction = $25.75
        2nd transaction =  $12.50
        3rd transaction = $7.25

        Expected output:
        Balance after 1st transaction = $74.25
        Balance after 2nd transaction = $61.75
        Balance after 3rd transaction = $54.5
        */

        Scanner input = new Scanner(System.in);

        System.out.println("User please enter your balance");
        double balance = input.nextDouble();
        System.out.println("The initial balance = $" + balance);

        System.out.println("User please enter your 1st transaction");
        System.out.println("Balance after 1st transaction = $" + (balance -= input.nextDouble()));

        System.out.println("User please enter your 2nd transaction");
        System.out.println("Balance after 1st transaction = $" + (balance -= input.nextDouble()));

        System.out.println("User please enter your 3rd transaction");
        System.out.println("Balance after 1st transaction = $" + (balance -= input.nextDouble()));




    }
}
