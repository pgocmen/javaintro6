package operators.arithmetic_operators;

public class MathFunctions {
    public static void main(String[] args) {

        int num1 = 9, num2 = 3;

        int sum = num1 + num2;

        System.out.println(sum);

        System.out.println("The sum of " + num1 + " and " + num2 + " is " + (num1+num2)); // 12
        System.out.println("The subtraction of " + num1 + " and " + num2 + " is " + (num1-num2)); // 6
        System.out.println("The multiplication of " + num1 + " and " + num2 + " is " + (num1*num2)); // 27
        System.out.println("The division of " + num1 + " and " + num2 + " is " + (num1/num2)); // 3
        System.out.println("The remainder of " + num1 + " and " + num2 + " is " + (num1%num2)); // 0
    }
}
