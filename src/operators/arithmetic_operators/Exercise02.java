package operators.arithmetic_operators;

public class Exercise02 {
    public static void main(String[] args) {

        double salary = 90000;

        System.out.println("The monthly income of an SDET is $ " + (salary / 12));
        System.out.println("The weekly income of an SDET is $ " + (salary / 52));
        System.out.println("The bi-weekly income of an SDET is $ " + (salary / 26));


    }
}
