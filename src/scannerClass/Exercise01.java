package scannerClass;

import java.util.Scanner;

public class Exercise01 {
    public static void main(String[] args) {
        /*
        Write a program that
         */

        Scanner input = new Scanner(System.in);

        String fName, address;


        System.out.println("User please enter your first name");
        fName = input.next();
        input.nextLine(); // empty line to skip the problem of Scanner

        System.out.println("User please enter your address");
        address = input.nextLine();

        System.out.println("User please enter your favorite number");
        int favNum = input.nextInt();

        System.out.println(fName + "'s address is " + address + " and their favorite number is " + favNum + ".");

    }
}
