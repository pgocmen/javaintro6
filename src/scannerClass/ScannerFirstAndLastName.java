package scannerClass;

import java.util.Scanner;

public class ScannerFirstAndLastName {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String fName, lName, address;

        System.out.println("Please enter your first name");
        fName = input.next();

        System.out.println("Please enter your address");
        address = input.nextLine();


        System.out.println("Please enter your last name");
        lName = input.next();



        System.out.println("Your full name is " + fName + " " + lName);
        System.out.println("Address: " + address);
    }
}
