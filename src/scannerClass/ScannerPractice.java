package scannerClass;

import java.util.Scanner;

public class ScannerPractice {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Enter your name ");
        String name = input.next();

        System.out.println("Enter your favorite number");
        int fNum = input.nextInt();

        System.out.println("Enter your favorite color");
        String color = input.next();

        System.out.println("Enter how many cars you have");
        int car = input.nextInt();

    }
}
