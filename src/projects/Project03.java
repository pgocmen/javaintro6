package projects;

public class Project03 {
    public static void main(String[] args) {
        System.out.println("---------------Task-1-----------------\n");

        String s1 = "24", s2 = "5";

        int i1 = Integer.parseInt(s1);
        int i2 = Integer.parseInt(s2);

        System.out.println("The sum of " + i1 + " and " + i2 + " = " + (i1 + i2));
        System.out.println("The subtraction of " + i1 + " and " + i2 + " = " + (i1 - i2));
        System.out.println("The division of " + i1 + " and " + i2 + " = " + ((double)i1 / (double)i2));
        System.out.println("The multiplication of " + i1 + " and " + i2 + " = " + (i1 * i2));
        System.out.println("The remainder of " + i1 + " and " + i2 + " = " + (i1 % i2));


        System.out.println("---------------Task-2-----------------\n");

        int ranNum = (int) (Math.random() * 35) + 1;

        if (ranNum == 1){
            System.out.println(ranNum + " IS NOT A PRIME NUMBER");
        }
        else if (ranNum == 2 || ranNum == 3 || ranNum == 5 || ranNum % 2 != 0 && ranNum % 3 != 0 && ranNum % 5 != 0){
            System.out.println(ranNum + " IS A PRIME NUMBER");
        }
       else {
            System.out.println(ranNum + " IS NOT A PRIME NUMBER");
        }



        System.out.println("---------------Task-3-----------------\n");

        int ran1 = 1 + (int) (Math.random() * 50);
        int ran2 = 1 + (int) (Math.random() * 50);
        int ran3 = 1 + (int) (Math.random() * 50);

        int lowest = Math.min(Math.min(ran1, ran2), ran3);
        int greatest = Math.max(Math.max(ran1, ran2), ran3);
        int middle = 0;

        if (lowest == ran1){
            middle = Math.min(ran2, ran3);
        } else if (lowest == ran2) {
            middle = Math.min(ran1, ran3);
        } else middle = Math.min(ran1, ran2);

        System.out.println("Lowest number is = " + lowest);
        System.out.println("Middle number is = " + middle);
        System.out.println("Greatest number is = " + greatest);

        System.out.println("---------------Task-4-----------------\n");

        char c1 = '3';

        boolean isLower1 = (c1 >= 'a' && c1 <= 'z');
        boolean isUpper1 = (c1 >= 'A' && c1 <= 'Z');

        if (isLower1 || isUpper1) {
            if (isLower1) System.out.println("The letter is lowercase");
            else System.out.println("The letter is uppercase");
        } else System.out.println("Invalid character detected!!!");

        System.out.println("---------------Task-5-----------------\n");
        char c2 = 'e';

        boolean isLower2 = (c2 >= 'a' && c2 <= 'z');
        boolean isUpper2 = (c2 >= 'A' && c2 <= 'Z');

        if (isLower2 || isUpper2) {
            if (isUpper2) c2 += 32; // 69 + 32 = 101 -> e
            if (c2 == 'a' || c2 == 'e' || c2 == 'i' || c2 == 'o' || c2 == 'u') {
                System.out.println("The letter is vowel");
            } else System.out.println("The letter is consonant");
        } else System.out.println("Invalid character detected!!!");


        System.out.println("---------------Task-6-----------------\n");
        char chars = '#';

        boolean isLetter = (chars >= 65 && chars <= 90) || (chars >= 97 && chars <= 122);
        boolean isDigit = chars >= 48 && chars <= 57;

        if (isLetter || isDigit) System.out.println("Invalid character detected!!!");
        else System.out.println("Special character is = " + chars);

        System.out.println("---------------Task-7-----------------\n");
        char char2 = '#';

        isLetter = (char2 >= 65 && char2 <= 90) || (char2 >= 97 && char2 <= 122);
        isDigit = char2 >= 48 && char2 <= 57;

        if (isLetter) System.out.println("Character is a letter");
        else if (isDigit) {
            System.out.println("Character is a digit");
        }
        else System.out.println("Character is a special character");
    }
}
