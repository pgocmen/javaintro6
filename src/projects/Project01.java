package projects;

public class Project01 {
    public static void main(String[] args) {

        System.out.println("------------------TASK-1--------------------\n");
        String name =  "Pinar";
        System.out.println("My name is = " + name);



        System.out.println("\n------------------TASK-2--------------------\n");
        char nameCharacter1 = 'P', nameCharacter2 = 'i', nameCharacter3 = 'n', nameCharacter4 = 'a',
                nameCharacter5 = 'r';

        System.out.println("Name letter 1 is = " + nameCharacter1);
        System.out.println("Name letter 2 is = " + nameCharacter2);
        System.out.println("Name letter 3 is = " + nameCharacter3);
        System.out.println("Name letter 4 is = " + nameCharacter4);
        System.out.println("Name letter 5 is = " + nameCharacter5);



        System.out.println("\n------------------TASK-3--------------------\n");
        String myFavMovie = "Big Daddy", myFavSong = "Flowers", myFavCity = "Chicago", myFavActivity = "Driving",
                myFacSnack = "Chocolate";

        System.out.println("My Favorite Movie is = " + myFavMovie);
        System.out.println("My Favorite Song is = " + myFavSong);
        System.out.println("My Favorite City is = " + myFavCity);
        System.out.println("My Favorite Activity is = " + myFavActivity);
        System.out.println("My Favorite Snack is = " + myFacSnack);


        System.out.println("\n------------------TASK-4--------------------\n");

        int myFavNumber = 5, numberOfStatesIVisited = 2, numberOfCountriesIVisited = 2;

        System.out.println("My favorite number is = " + myFavNumber);
        System.out.println("The number of states i have visited is = " + numberOfStatesIVisited);
        System.out.println("The number of countries i have visited is = " + numberOfCountriesIVisited);


        System.out.println("\n------------------TASK-5--------------------\n");

        boolean amIAtSchoolToday = false;

        System.out.println("I am at school today = " + amIAtSchoolToday);



        System.out.println("\n------------------TASK-6--------------------");

        boolean isWeatherNiceToday = false;

        System.out.println("Weather is nice today = " + isWeatherNiceToday);




    }
}
