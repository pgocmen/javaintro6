package projects;

import utilities.RandomNumber;
import utilities.ScannerHelper;

import java.util.Scanner;

public class Project04 {
    public static void main(String[] args) {

        System.out.println("-----------------------------Task-1------------------------------");

        String name = ScannerHelper.getAName();

        if (name.length() < 8) {
            System.out.println("This String does not have 8 characters");
        }else System.out.println(name.substring(name.length() - 4)
                + name.substring(4, name.length() - 4)
                + name.substring(0, 4));

        System.out.println("-----------------------------Task-2------------------------------");

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a sentence.");
        String sentence = input.nextLine();
        sentence = sentence.trim();
        int count = 0;
        for (int i = 0; i <sentence.length()-1 ; i++) {
            if(sentence.charAt(i)==' ') count++;

        }if (count<1) System.out.println("This sentence does not have 2 or more words to swap");
        else System.out.println(sentence.substring(sentence.lastIndexOf(' ')+1) +
                    sentence.substring(sentence.indexOf(' '), sentence.lastIndexOf(' ')+1) +
                    sentence.substring(0, sentence.indexOf(' ')));

        System.out.println("-----------------------------Task-3------------------------------");

        String str = ScannerHelper.getAString();

        if (str.contains("stupid") || str.contains("idiot"));{
            str = str.replace("stupid", "nice");
            str = str.replace("idiot", "nice");
        } System.out.println(str);

        System.out.println("-----------------------------Task-4------------------------------");


        String name2 = ScannerHelper.getAName();

        if (name2.length() < 2) {
            System.out.println("Invalid input!!!");
        } else if
        (name2.length() % 2 == 0){
            System.out.println(name2.substring(name2.length()/2-1, name2.length()/2+1));
        }else System.out.println(name2.charAt(name2.length()/2));

        System.out.println("-----------------------------Task-5------------------------------");

        System.out.println("User please enter a country");
        String country = input.next();

        if (country.length() < 5){
            System.out.println("Invalid Input!!");
        }else
            System.out.println(country.substring(2, country.length()-2));

        System.out.println("-----------------------------Task-6------------------------------");


        String address = ScannerHelper.getAnAddress();
        String lower = address.toLowerCase();
        String replace = "";
        for (int i = 0; i < address.length(); i++) {
            if (lower.charAt(i)=='a') replace = replace +  '*';
            else if (lower.charAt(i)=='e') replace = replace +  '#';
            else if (lower.charAt(i)=='i') replace = replace +  '+';
            else if (lower.charAt(i)=='o') replace = replace +  '$';
            else if (lower.charAt(i)=='u') replace = replace +  '@';
            else replace = replace + address.charAt(i);
        }
        System.out.println(replace);
    }}