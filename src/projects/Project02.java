package projects;

import java.util.Scanner;

public class Project02 {

    public static void main(String[] args){

        System.out.println("------------------TASK-1--------------------");

        Scanner input = new Scanner(System.in);

        System.out.println("User please enter 3 numbers");
        int num1 = input.nextInt();
        int num2 = input.nextInt();
        int num3 = input.nextInt();

        System.out.println("The product of the numbers entered is = " + (num1 * num2 * num3));


        System.out.println("\n------------------TASK-2--------------------\n");

        System.out.println("What is your first name?");
        String fName = input.next();

        System.out.println("What is your last name?");
        String lName = input.next();

        System.out.println("What is your year of birth");
        int bYear = input.nextInt();
        input.nextLine();

        System.out.println(fName + " " + lName + "'s age is = " + (2023 - bYear));


        System.out.println("\n------------------TASK-3--------------------\n");

        System.out.println("What is your full name?");
        String fullName = input.nextLine();

        System.out.println("What is your weight in kilograms?");
        int weight = input.nextInt();
        input.nextLine();

        System.out.println(fullName + "'s weight is = " + (weight * 2.205) + "lbs.");

        System.out.println("\n------------------TASK-4--------------------\n");

        System.out.println("What is your full name?");
        String name1 = input.nextLine();

        System.out.println("What is your age?");
        int age1 = input.nextInt();
        input.nextLine();

        System.out.println("What is your full name?");
        String name2 = input.nextLine();

        System.out.println("What is your age?");
        int age2 = input.nextInt();
        input.nextLine();

        System.out.println("What is your full name?");
        String name3 = input.nextLine();

        System.out.println("What is your age?");
        int age3 = input.nextInt();

        System.out.println(name1 + "'s age is " + age1 + ".");
        System.out.println(name2 + "'s age is " + age2 + ".");
        System.out.println(name3 + "'s age is " + age3 + ".");
        System.out.println("The average age is " + (age1 + age2 + age3) / 3);
        System.out.println("The eldest age is " + Math.max(Math.max(age1, age2) , age3));
        System.out.println("The youngest age is " + Math.min(Math.min(age1, age2) , age3));

    }
}
