package projects;

import utilities.RandomNumber;

import java.util.Scanner;

public class Project05 {
    public static void main(String[] args) {

        System.out.println("----------------Task-1-------------------------\n");

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a sentence:");
        input.nextLine();
        String str1 = input.nextLine().trim();

        if (str1.contains(" ")) {
            String[] array8 = str1.split(" ");
            System.out.println("This sentence has " + array8.length + " words.");
        } else {
            System.out.println("This sentence does not have multiple words.");
        }

        System.out.println("----------------Task-2-------------------------\n");

        int numA = RandomNumber.getARandomNumber(0, 25);
        int numB = RandomNumber.getARandomNumber(0, 25);

        String str2 = "";

        int min = Math.min(numA, numB);
        int max = Math.max(numA, numB);

        for (int i = min; i <= max; i++) {
            if (i % 5 != 0) {
                str2 += i;
                if (i + 1 == max && i % 5 != 0) break;
                else {
                    str2 += " - ";
                }
            }
        }

        System.out.println("----------------Task-3-------------------------\n");

        System.out.println("Please input a string:");
        String str3 = input.nextLine();
        int counter = 0;

        if (str3.length() == 0 || (!str3.contains("a") && !str3.contains("A"))) {
            System.out.println("This sentence does not have any characters.");
        }
        else {
            for (int i = 0; i < str3.length(); i++) {
                if (str3.charAt(i) == 'a' || str3.charAt(i) == 'A') counter++;
            }
            System.out.println("This sentence has " + counter + " a or A letters.");
        }


        System.out.println("----------------Task-4-------------------------\n");

        System.out.println("Please enter a string: ");
        String str4 = input.nextLine();
        boolean isPalindrome = true;

        if (str4.length() < 1) {
            System.out.println("This word does not have 1 or more characters.");
        } else {
            for (int i = 0, j = str4.length() - 1; i <= str4.length() / 2; i++, j--) {
                if (str4.charAt(i) == str4.charAt(j)) {
                    isPalindrome = false;
                    break;
                }
            }
            if (isPalindrome) System.out.println("This word is a palindrome");
            else System.out.println("This word is not a palindrome");
        }


        System.out.println("----------------Task-5-------------------------\n");

        for (int i = 1; i <= 18; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println();

        }


    }
}

