package conditional_statements;

public class IfElseSyntax {
    public static void main(String[] args) {
        /*

         */

        boolean condition = true;

        if(condition){
            // This is if block and will execute when the condition is true
            System.out.println("A");
        }
        else {
            // This is else block and will execute when condition is false
            System.out.println("B");

        }

        System.out.println("End of the program");
    }
}
