package conditional_statements;

import java.util.Scanner;

public class Exercise03_PosNegZero {
    public static void main(String[] args) {

        /*
        Write a program that asks user to enter a number
        if the number is more than zero, print "POSITIVE"
        if the number is less than zero, print "NEGATIVE"
        Otherwise, print "ZERO"
         */

        Scanner input = new Scanner(System.in);

        System.out.println("User please enter a number");
        int num = input.nextInt();

        if (num > 0){
            System.out.println("POSITIVE");
        } else if (num < 0 ) {
            System.out.println("NEGATIVE");
        }
        else {
            System.out.println("ZERO");
        }

        System.out.println("End of Program");
    }
}
