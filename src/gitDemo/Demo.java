package gitDemo;

public class Demo {
    public static void main(String[] args) {
        String firstName = "Pinar";
        String lastName = "Gocmen";

        System.out.println(firstName + " " + lastName);

        String facColor = "Black";
        System.out.println("My favorite color is = " + facColor);

        System.out.println("New Line");


        String favoriteCar = "Porsche";
        int year = 2023;

        System.out.println("My favorite car is " + favoriteCar + "and the model year is " + year);

    }
}
