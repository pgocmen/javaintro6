package gitDemo;

public class MergeConflict {
    public static void main(String[] args) {

        String favoriteColor = "Black";
        int shades = 5;

        System.out.println("My favorite color is " + favoriteColor + " and the number of shades of " + favoriteColor +
        " is " + shades);
    }
}
